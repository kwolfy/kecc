package kecc

import (
	"os"
	"bufio"
	"time"
	"io"
	"fmt"
	"github.com/steakknife/keccak"
)

func GetSeeds(filename string) [][]byte {
	seeds := make([][]byte, 0)
	f, err := os.Open(filename);
	check(err)
	defer f.Close()
	r := bufio.NewReader(f)

	start := time.Now()

	for {
		seed, err := r.ReadString(10) // 0x0A separator = newline
		if err == io.EOF {
			b := []byte(seed)
			seeds = append(seeds, b)

			f.Close();
			// do something here
			break
		} else if err != nil {
			check(err)
		} else {
			b := []byte(seed)
			seeds = append(seeds, b[:len(b) - 1])
		}
	}

	fmt.Println("gettings seeds: ", time.Since(start));

	return seeds
}

func HashKeccak(inp []byte) []byte {
	h := keccak.New256()
	h.Write(inp)
	hash := h.Sum(nil)

	return hash;
}