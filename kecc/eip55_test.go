package kecc

import (
	"testing"
	"github.com/stretchr/testify/assert"
)


type AddrItem struct {
	In string
	Out string
}

var addrs = []AddrItem {
	{"0xd3cda913deb6f67967b99d67acdfa1712c293601", "0xd3CdA913deB6f67967B99D67aCDFa1712C293601"},
	{"0xD3CDA913DEB6F67967B99D67ACDFA1712C293601", "0xd3CdA913deB6f67967B99D67aCDFa1712C293601"},
	{"0xd3CdA913deB6f67967B99D67aCDFa1712C293601", "0xd3CdA913deB6f67967B99D67aCDFa1712C293601"},
	{"0xfb6916095ca1df60bb79ce92ce3ea74c37c5d359", "0xfB6916095ca1df60bB79Ce92cE3Ea74c37c5d359"},
}

func TestToChecksumAddressEip55(t *testing.T) {
	for i := 0; i < len(addrs); i++ {
		addr := addrs[i]
		assert.Equal(t, ToChecksumAddressEip55(addr.In), addr.Out)
	}
}

