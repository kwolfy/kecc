package kecc

import (
	"flag"
	"fmt"
	"os"
	"math"
	"crypto/sha256"
	"github.com/btcsuite/btcd/btcec"
	"runtime"
	"golang.org/x/crypto/ripemd160"
	"github.com/btcsuite/btcutil/base58"
	"time"
)


func exit(out string) {
	fmt.Println(out)
	os.Exit(1);
}


type ElectrumResult struct {
	id int
	results []E256KeyPair
}


func RunElectrum() {
	start := time.Now()
	runElectrum()
	fmt.Println("time: ", time.Since(start))
}

func runElectrum() {
	privateKeyFlag := flag.String("p", "", "-p private_key_file.txt")
	privateKeyCompressedFlag := flag.String("pu", "", "-pu private_key_c_file.txt")
	uncompressedFlag := flag.String("u", "", "-u uncompressed_file.txt")
	compressedFlag := flag.String("c", "", "-c compressed_file.txt")
	threadsFlag := flag.Int("t", runtime.NumCPU(),"-t [number]")

	flag.Parse()

	if(len(flag.Args()) < 1) {
		exit("Использование: electrum [-p файл с приватными ключами] [-pu файл с приватными ключами compressed] [-c файл с compressed] [-u файд с uncompressed] [-t количество потоков] [входной_файл]")
	}

	shouldSavePrivateKeys := len(*privateKeyFlag) > 0
	shouldSavePrivateCompressedKeys := len(*privateKeyCompressedFlag) > 0
	shouldSaveUncompressed := len(*uncompressedFlag) > 0
	shouldSaveCompressed := len(*compressedFlag) > 0

	inFilename := flag.Arg(0);

	seeds := GetSeeds(inFilename)
	fmt.Println("total seeds: ", len(seeds))

	totalChunks := *threadsFlag;
	chunksCount := int(math.Ceil(float64(len(seeds)) / float64(totalChunks)));

	results := make(map[int][]E256KeyPair)
	msg := make(chan ElectrumResult)

	for i := 0; i < totalChunks; i++ {
		offset := (i + 1) * chunksCount - chunksCount
		end := (i + 1) * chunksCount
		go proceessSeedsElectrum(msg, i, seeds[offset:end])
	}

	for i := 0; i < totalChunks; i++ {
		result := <- msg
		results[result.id] = result.results
	}


	fs := make(map[string]*os.File)

	if shouldSaveCompressed {
		f, err := os.Create(*compressedFlag)
		check(err)
		fs["compressed"] = f;
		defer f.Sync()
		defer f.Close()
	}

	if shouldSavePrivateCompressedKeys {
		f, err := os.Create(*privateKeyCompressedFlag)
		check(err)
		fs["pkcompressed"] = f;
		defer f.Sync()
		defer f.Close()
	}

	if shouldSavePrivateKeys {
		f, err := os.Create(*privateKeyFlag)
		check(err)
		fs["privatekey"] = f;
		defer f.Sync()
		defer f.Close()
	}

	if shouldSaveUncompressed {
		f, err := os.Create(*uncompressedFlag)
		check(err)
		fs["uncompressed"] = f;
		defer f.Sync()
		defer f.Close()
	}

	for i := 0; i < totalChunks; i++ {
		for _, item := range results[i] {
			if shouldSaveCompressed {
				fs["compressed"].WriteString(item.GetBitcoinAddress(true) + "\n")
			}

			if shouldSavePrivateCompressedKeys {
				fs["pkcompressed"].WriteString(item.PrivateKeyToString(true) + "\n")
			}

			if shouldSaveUncompressed {
				fs["uncompressed"].WriteString(item.GetBitcoinAddress(false) + "\n")
			}

			if shouldSavePrivateKeys {
				fs["privatekey"].WriteString(item.PrivateKeyToString(false) + "\n")
			}
		}
	}
}

func proceessSeedsElectrum (msg chan ElectrumResult, id int, seeds [][]byte) {
	addresses := make([]E256KeyPair, len(seeds))
	for i := 0; i < len(seeds); i++ {
		addresses[i] = genKeyPair(seeds[i])
	}

	msg <- ElectrumResult {id, addresses}
}


type KeyPair struct {
	PrivateKey string
	Address string
	CompressedPrivateKey string
	CompressedAddress string
}

func GenerateKeyPair(passphrase string, compressed bool) KeyPair {
	keys := genKeyPair([]byte(passphrase))

	return KeyPair{
		PrivateKey: keys.PrivateKeyToString(compressed),
		Address: keys.GetBitcoinAddress(compressed)}
}

func genKeyPair(passphrase []byte) E256KeyPair {
	hash := sha256.Sum256([]byte(passphrase))
	sk, pk := btcec.PrivKeyFromBytes(btcec.S256(), hash[:])

	return E256KeyPair{*pk, *sk}
}


type E256KeyPair struct {
	Pk btcec.PublicKey
	Sk btcec.PrivateKey
}

func (k E256KeyPair) PrivateKeyToString(compressed bool) string {
	sk := k.Sk.Serialize()


	if compressed {
		sk = append(sk, 0x01)
	}

	return keyToString(sk, 128)
}


func (k E256KeyPair) GetBitcoinAddress(compressed bool) string {
	var pk []byte
	if compressed {
		pk = k.Pk.SerializeCompressed()
	} else {
		pk = k.Pk.SerializeUncompressed()
	}

	pubHash := pubKeyHash(pk)
	return keyToString(pubHash, 0x00)
}

/*
if (this.pubKeyHash) return this.pubKeyHash;

    return this.pubKeyHash = Bitcoin.Util.sha256ripe160(this.getPub());
 */

func pubKeyHash(src []byte) []byte {
	shaHash := sha256.Sum256(src)
	h := ripemd160.New()

	h.Write(shaHash[:])

	hash := h.Sum(nil)
	return hash[:]
}

func keyToString(key []byte, version byte) string {
	address := append([]byte{version}, key...)
	checksum := sha256.Sum256(address)
	checksum = sha256.Sum256(checksum[:])

	address = append(address, checksum[:4]...)

	return base58.Encode(address)
}



