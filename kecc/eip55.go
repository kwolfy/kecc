package kecc

import (
	"strings"
	"encoding/hex"
	"strconv"
	"time"
	"fmt"
	"flag"
	"runtime"
	"math"
	"os"
)



func RunEIP55() {
	start := time.Now()
	runEIP55()
	fmt.Println("time: ", time.Since(start))
}

type EIP55Result struct {
	Normal string
	Upper string
	Lower string
}


type EIP55MsgResult struct {
	id int
	results []EIP55Result
}


func runEIP55() {
	checksumFlag := flag.String("c", "", "-c normal_file.txt")
	upperFlag := flag.String("u", "", "-u upper_file.txt")
	lowerFlag := flag.String("l", "", "-l lower_file.txt")
	threadsFlag := flag.Int("t", runtime.NumCPU(),"-t [number]")

	flag.Parse()

	if(len(flag.Args()) < 1) {
		exit("Использование: eip55 [-c файл normal checksum] [-u файл с upper checksum] [-c файл с lower checksum] [-t количество потоков] [входной_файл]")
	}

	runtime.GOMAXPROCS(*threadsFlag)

	needNormal := len(*checksumFlag) > 0
	needUpper := len(*upperFlag) > 0
	needLower := len(*lowerFlag) > 0

	inFilename := flag.Arg(0);

	seeds := GetSeeds(inFilename)
	fmt.Println("total seeds: ", len(seeds))

	totalChunks := *threadsFlag;
	chunksCount := int(math.Ceil(float64(len(seeds)) / float64(totalChunks)));

	results := make(map[int][]EIP55Result)
	msg := make(chan EIP55MsgResult)

	for i := 0; i < totalChunks; i++ {
		offset := (i + 1) * chunksCount - chunksCount
		end := (i + 1) * chunksCount
		go processEip55Seeds(msg, i, seeds[offset:end], needNormal, needUpper, needLower)
	}

	for i := 0; i < totalChunks; i++ {
		result := <- msg
		results[result.id] = result.results
	}


	fs := make(map[string]*os.File)

	if needNormal {
		f, err := os.Create(*checksumFlag)
		check(err)
		fs["normal"] = f;
		defer f.Sync()
		defer f.Close()
	}

	if needUpper {
		f, err := os.Create(*upperFlag)
		check(err)
		fs["upper"] = f;
		defer f.Sync()
		defer f.Close()
	}

	if needLower {
		f, err := os.Create(*lowerFlag)
		check(err)
		fs["lower"] = f;
		defer f.Sync()
		defer f.Close()
	}

	for i := 0; i < totalChunks; i++ {
		for _, item := range results[i] {
			if needNormal {
				fs["normal"].WriteString(item.Normal + "\n")
			}

			if needUpper {
				fs["upper"].WriteString(item.Upper + "\n")
			}

			if needLower {
				fs["lower"].WriteString(item.Lower + "\n")
			}
		}
	}
}

func processEip55Seeds(msg chan EIP55MsgResult, id int, seeds [][]byte, normal bool, upper bool, lower bool) {
	results := make([]EIP55Result, len(seeds))
	for i := 0; i < len(seeds); i++ {
		item := EIP55Result{}

		item.Normal = ToChecksumAddressEip55(string(seeds[i]))

		if upper {
			item.Upper = ToChecksumAddressEip55Upper(item.Normal)
		}

		if lower {
			item.Lower = ToChecksumAddressEip55Lower(item.Normal)
		}

		results[i] = item
	}

	msg <- EIP55MsgResult {id, results}
}



func ToChecksumAddressEip55Lower(checksum string) string {
	return checksum[:2] + strings.ToLower(checksum[2:]);
}


func ToChecksumAddressEip55Upper(checksum string) string {
	return checksum[:2] + strings.ToUpper(checksum[2:]);
}


func ToChecksumAddressEip55(addr string) string {
	addr = strings.ToLower(addr)
	addr = strings.Replace(addr, "0x", "", -1)
	hash := HashKeccak([]byte(addr))
	hashHex := hex.EncodeToString(hash)

	ret := "0x"

	for i := 0; i < len(addr); i++ {
		b, _ := strconv.ParseInt(hashHex[i:i+1], 16, 32)

		if(b >= 8) {
			ret = ret + strings.ToUpper(addr[i:i+1])
		} else {
			ret = ret + addr[i:i+1]
		}
	}

	return ret
}


