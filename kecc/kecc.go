package kecc


import (
	"fmt"
	"os"
	"time"
	"math"
	"strings"
	"runtime"
	"flag"
	"golang.org/x/crypto/blake2b"
	"crypto/sha256"
	"golang.org/x/crypto/curve25519"
	b58 "github.com/jbenet/go-base58"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	RunKecc();
}

func RunKecc(){
	start := time.Now()
	run()
	fmt.Println("all: ", time.Since(start))
}


type ProcessResult struct {
	id int
	results []string
}

func run() {
	threadsFlag := flag.Int("t", runtime.NumCPU(), "")
	flag.Parse()

	if(len(flag.Args()) < 3) {
		fmt.Println("Использование: waves [входной_файл] [выходной_файл] [-t количество потоков]")
		os.Exit(1);
	}

	inFilename := flag.Arg(0);
	outFilename := flag.Arg(1);

	seeds := GetSeeds(inFilename)
	fmt.Println("total seeds: ", len(seeds))

	totalChunks := *threadsFlag;
	chunksCount := int(math.Ceil(float64(len(seeds)) / float64(totalChunks)));

	results := make(map[int][]string)
	msg := make(chan ProcessResult)

	for i := 0; i < totalChunks; i++ {
		offset := (i + 1) * chunksCount - chunksCount
		end := (i + 1) * chunksCount
		go proceessSeeds(msg, i, seeds[offset:end])
	}

	for i := 0; i < totalChunks; i++ {
		result := <- msg
		results[result.id] = result.results
	}

	f, err := os.Create(outFilename)
	check(err);



	for i := 0; i < totalChunks; i++ {
		if i != 0 {
			f.WriteString("\n")
		}
		f.WriteString(strings.Join(results[i], "\n"))
	}

	f.Sync();
	f.Close()
}

func proceessSeeds (msg chan ProcessResult, id int, seeds [][]byte) {
	addresses := make([]string, len(seeds))
	for i := 0; i < len(seeds); i++ {
		addresses[i] = BuildAddressFromSeed(seeds[i])
	}

	msg <- ProcessResult {id, addresses}
}


type keyPair struct {
	Pk []byte
	Sk []byte
}


func BuildAddressFromSeed(seed []byte) string {
	keys := GenerateKeyPairFromSeed(seed)
	pkHash := HashChain(keys.Pk)
	prefix := []byte{1, 87}

	unhashedAddress := append(prefix[:], pkHash[:20]...)
	addressHash := HashChain(unhashedAddress)[:4]

	raw := append(unhashedAddress[:], addressHash[:]...)

	return b58.Encode(raw)
}

func GenerateKeyPairFromSeed(seed []byte) keyPair {
	seedHash := BuildSeedHash(seed);
	sk := [32]byte{}
	pk := [32]byte{}

	for i := 0; i < 32; i++ {
		sk[i] = seedHash[i];
	}

	curve25519.ScalarBaseMult(&pk, &sk);

	return keyPair{pk[:], sk[:]}
}



var fixedNonce = []byte{0, 0, 0, 0}

func BuildSeedHash(seed []byte) []byte {
	return HashSha256(HashChain(append(fixedNonce[:], seed[:]...)))
}


func HashChain(inp []byte) []byte {
	return HashKeccak(HashBlake2b(inp))
}

func HashBlake2b(inp []byte) []byte {
	blakeHash := blake2b.Sum256(inp)
	return blakeHash[:];
}


func HashSha256(inp []byte) []byte {
	h := sha256.New();
	h.Write(inp);

	return h.Sum(nil);
}